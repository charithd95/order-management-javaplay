name := """OrderManagement"""
organization := "com.nCinga"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.13.1"

libraryDependencies ++= Seq(
  guice,
  "org.pac4j" % "pac4j-mongo" % "3.8.3",
  "org.pac4j" % "pac4j-http" % "3.8.3",
  "org.mongodb" % "mongo-java-driver" % "3.8.2"
)
libraryDependencies += "com.typesafe.play" %% "play-json" % "2.8.1"
libraryDependencies += "org.apache.httpcomponents" % "httpasyncclient" % "4.1.2"
libraryDependencies += "com.google.code.gson" % "gson" % "2.8.3"
libraryDependencies += "log4j" % "log4j" % "1.2.17"
