package bo;

import com.fasterxml.jackson.databind.JsonNode;

import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class Order {
    private long orderId;
    private List<JsonNode> products;
    private int customerId;
    private LocalDate date;
    private String orderStatus;
    public Order(long orderId,List<JsonNode> products, int customerId, String orderStatus) {
        this.orderId = orderId;
        this.products = products;
        this.customerId = customerId;
        this.date = LocalDate.now();;
        this.orderStatus = orderStatus;
    }

    public long getOrderId() {
        return orderId;
    }

    public List<JsonNode> getProducts() {
        return products;
    }

    public int getCustomerId() {
        return customerId;
    }

    public LocalDate getDate() {
        return date;
    }

    public String getOrderStatus() {
        return orderStatus;
    }
}
