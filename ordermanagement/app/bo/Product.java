package bo;

public class Product {
    private String productName;
    private int availableQuantity;
    private double price;
    private long productId;

    public Product(String productName, int availableQuantity, double price,long productId) {
        this.productName = productName;
        this.availableQuantity = availableQuantity;
        this.price = price;
        this.productId=productId;
    }

    public String getProductName() {
        return productName;
    }

    public int getAvailableQuantity() {
        return availableQuantity;
    }

    public double getPrice() {
        return price;
    }

    public void setAvailableQuantity(int availableQuantity) {
        this.availableQuantity = availableQuantity;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
