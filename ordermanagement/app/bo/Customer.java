package bo;

import java.util.HashMap;

public class Customer {
    private long customerId;
    private String name;
    private HashMap<String,String> address;
    private int age;
    private boolean activeStatus;

    public Customer(String name,long customerId,String lineOne, String lineTwo, String streetNo, String landmark, String city, String country, String zipcode,int age,boolean activeStatus) {
        this.customerId=customerId;
        this.name=name;
        this.age=age;
        this.activeStatus = activeStatus;
        HashMap<String,String> address =new HashMap<>();
        address.put("AddressLineOne",lineOne);
        address.put("AddressLineTwo",lineTwo);
        address.put("StreetNo",streetNo);
        address.put("landmark",landmark);
        address.put("City",city);
        address.put("Country",country);
        address.put("zipcode",zipcode);
        this.address=address;
    }

    public Customer() {
    }

    public int getAge() {
        return age;
    }

    public boolean isActiveStatus() {
        return activeStatus;
    }


    public void setAge(int age) {
        this.age = age;
    }

    public void setActiveStatus(boolean activeStatus) {
        this.activeStatus = activeStatus;
    }
}
