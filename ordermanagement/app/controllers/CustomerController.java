package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import service.impl.CustomerServiceImpl;

import java.util.List;

public class CustomerController extends Controller {
    CustomerServiceImpl customerServiceImpl =new CustomerServiceImpl();
    public Result getCustomers(){
        List<Object> output = customerServiceImpl.getCustomers();
        return ok(Json.toJson(output));

    }
    public Result addCustomer(Http.Request request){
        JsonNode requestData=request.body().asJson();
        customerServiceImpl.addCustomer(requestData);
        return ok();
    }
    public Result deleteCustomer(Http.Request request){
        JsonNode requestData=request.body().asJson();
        customerServiceImpl.deleteCustomer(requestData);
        return ok();

    }

}
