package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import service.impl.ProductServiceImpl;

import java.util.List;

import static play.mvc.Results.ok;

public class ProductController extends Controller {
    ProductServiceImpl productServiceImpl =new ProductServiceImpl();
    public Result getProducts(){
        List<Object> output = productServiceImpl.getProducts();
        return ok(Json.toJson(output));

    }
    public Result addProduct(Http.Request request){
        JsonNode requestData=request.body().asJson();
        productServiceImpl.addProduct(requestData);
        return ok();

    }
    public Result deleteProduct(Http.Request request) {
        JsonNode requestData = request.body().asJson();
        productServiceImpl.deleteProduct(requestData);
        return ok();
    }

}
