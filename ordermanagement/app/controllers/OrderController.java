package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import service.impl.OrderServiceImpl;

public class OrderController extends Controller {
    OrderServiceImpl orderServiceImpl =new OrderServiceImpl();
    public Result placeAnOrder(Http.Request request){
        JsonNode requestData = request.body().asJson();
        orderServiceImpl.placeAnOrder(requestData);
        return ok();
    }
    public Result getOrdersByCustomerId(String id) {
        int  customerId=Integer.parseInt(id);
        return ok(orderServiceImpl.getOrdersByCustomerId(customerId));
    }
    public Result changeOrderStatus(Http.Request request){
        JsonNode requestData = request.body().asJson();
        orderServiceImpl.changeOrderStatus(requestData);
        return ok();
    }
}
