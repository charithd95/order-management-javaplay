package exceptions;

public class InactiveCustomerException extends RuntimeException {
    public InactiveCustomerException(String message){
        super(message);
    }
}
