package exceptions;

public class StatusChangeInvalidException extends RuntimeException {
    public StatusChangeInvalidException(String message){
        super(message);
    }
}
