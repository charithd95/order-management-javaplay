package dao;

import bo.Customer;
import com.fasterxml.jackson.databind.JsonNode;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import connection.DbConnection;
import org.bson.Document;
import play.libs.Json;
import java.util.ArrayList;
import java.util.List;

public class CustomerDao {
    MongoClient customerClient;
    MongoDatabase database;
    MongoCollection<Document> collection;
    MongoCursor<Document> cursor;

    public CustomerDao() {
        this.customerClient=DbConnection.getMongoClient();
        this.database=customerClient.getDatabase("order_management");
        this.collection= database.getCollection("customers");
        this.cursor = collection.find().iterator();
    }

    public void addCustomer(String name, String lineOne, String lineTwo, String streetNo, String landmark, String city, String country, String zipcode, int age){
        long customerId= (collection.countDocuments()+1);
        Customer newCustomer = new Customer(name,customerId,lineOne,lineTwo,streetNo,landmark,city,country,zipcode,age,true);
        JsonNode jsonNode= Json.toJson(newCustomer);
        Document document = Document.parse(jsonNode.toString());
        collection.insertOne(document);

    }
    public List<Object> getCustomers(){
        List<Object> customers = new ArrayList<>();
        try {
            while (cursor.hasNext()) {
                customers.add(cursor.next());

            }
        } finally {
        }
        return customers;


    }
    public void deleteCustomer(int Id){
        Document document=new Document();
        document.put("customerId", Id);
        collection.findOneAndDelete(document);

    }
    public Boolean isValidCustomer(int customerId){
        Document document=new Document();
        document.put("customerId", customerId);
        JsonNode jsonNode = Json.toJson(collection.find(document).first());
        Boolean currentStatus = jsonNode.get("activeStatus").asBoolean();
        return currentStatus;
    }


}
