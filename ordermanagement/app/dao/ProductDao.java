package dao;

import bo.Product;
import com.fasterxml.jackson.databind.JsonNode;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import connection.DbConnection;
import org.bson.Document;
import play.libs.Json;

import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Filters.eq;

public class ProductDao {
    MongoClient customerClient;
    MongoDatabase database;
    MongoCollection<Document> collection ;
    MongoCursor<Document> cursor ;

    public ProductDao() {
        this.customerClient = DbConnection.getMongoClient();
        this.database=customerClient.getDatabase("order_management");
        this.collection = database.getCollection("products");
        this.cursor = collection.find().iterator();

    }

    public void addProduct(String name, int quantity, double price){
        long productId= (collection.countDocuments()+1);
        Product newProduct = new Product(name,quantity,price,productId);
        JsonNode jsonNode= Json.toJson(newProduct);
        Document document = Document.parse(jsonNode.toString());
        collection.insertOne(document);
    }
    public List<Object> getProducts(){
        List<Object> products = new ArrayList<>();
        try {
            while (cursor.hasNext()) {
                products.add(cursor.next());

            }
        } finally {
        }
        return products;
    }
    public void deleteProduct(int Id){
        Document document=new Document();
        document.put("productId", Id);
        collection.findOneAndDelete(document);

    }
    public void decreaseProductCount(int productId,int quantity){

        Document document=new Document();
        document.put("productId", productId);
        JsonNode jsonNode = Json.toJson(collection.find(document).first());
        int availableQuantity = jsonNode.get("availableQuantity").asInt();
        int newQuantity = availableQuantity-quantity;
        collection.updateOne(eq("productId", productId), new Document("$set", new Document("availableQuantity", newQuantity)));


    }
    public int getProductCount(int productId){
        Document document=new Document();
        document.put("productId", productId);
        JsonNode jsonNode = Json.toJson(collection.find(document).first());
        int availableQuantity = jsonNode.get("availableQuantity").asInt();
        return availableQuantity;

    }
}
