package dao;

import bo.Order;
import bo.OrderStatus;
import bo.Product;
import com.fasterxml.jackson.databind.JsonNode;
import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import connection.DbConnection;
import org.bson.Document;
import play.libs.Json;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.mongodb.client.model.Filters.eq;

public class OrderDao {
    MongoClient customerClient;
    MongoDatabase database;
    MongoCollection<Document> collection;
    MongoCursor<Document> cursor;

    public OrderDao() {
        this.customerClient = DbConnection.getMongoClient();
        this.database = customerClient.getDatabase("order_management");
        this.collection = database.getCollection("orders");
        this.cursor = collection.find().iterator();
    }

    public void placeAnOrder(List<JsonNode> products, int customerId, String orderStatus){
        long orderId= (collection.countDocuments()+1);
        Order newOrder = new Order(orderId,products,customerId,orderStatus);
        JsonNode jsonNode = Json.toJson(newOrder);
        Document document = Document.parse(jsonNode.toString());
        collection.insertOne(document);

    }
    public JsonNode findByCustomerId(int id){
        List<Object> orders=new ArrayList<>();
        long customerId=id;
        while (cursor.hasNext()) {
            Document document = cursor.next();
            JsonNode jsonNode = Json.toJson(document);
            if(jsonNode.get("customerId").asLong()==id){
                orders.add(document);
            }
        }
        return (Json.toJson(orders));

    }
    public void changeOrderStatus(int orderId,String orderStatus){
        Document document=new Document();
        document.put("orderId", orderId);
        JsonNode jsonNode = Json.toJson(collection.find(document).first());
        collection.updateOne(eq("orderId", orderId), new Document("$set", new Document("orderStatus", orderStatus)));
    }
    public String getCurrentStatus(int orderId){
        Document document=new Document();
        document.put("orderId", orderId);
        JsonNode jsonNode = Json.toJson(collection.find(document).first());
        String currentStatus = jsonNode.get("orderStatus").asText();
        return currentStatus;
    }
}