package connection;

import com.mongodb.MongoClient;

public class DbConnection {
    private static MongoClient mongoClient;
    //private static DbConnection dbConnection;

    public static MongoClient getMongoClient() {
        mongoClient=mongoClient==null ? new MongoClient(): mongoClient;
        return  mongoClient;
    }
//    public static DbConnection getInstance() {
//        dbConnection=dbConnection==null ? new DbConnection(): dbConnection;
//        return  dbConnection;
//    }
}
