package service.impl;

import bo.OrderStatus;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.inject.internal.cglib.core.$Customizer;
import dao.CustomerDao;
import dao.OrderDao;
import dao.ProductDao;
import exceptions.InactiveCustomerException;
import exceptions.ProductNotAvailableException;
import exceptions.StatusChangeInvalidException;
import org.checkerframework.checker.units.qual.C;
import org.apache.log4j.Logger;
import java.util.ArrayList;
import java.util.List;

public class OrderServiceImpl {
    OrderDao orderDao;
    ProductDao productDao;
    CustomerDao customerDao;
    Logger logger;

    public OrderServiceImpl() {
        this.customerDao=new CustomerDao();
        this.orderDao = new OrderDao();
        this.productDao = new ProductDao();
        this.logger = Logger.getLogger(OrderServiceImpl.class);

    }

    public void placeAnOrder(JsonNode requestData){
        ArrayNode products=(ArrayNode) requestData.get("Products");
        List<JsonNode> productsList=new ArrayList<>();
        int customerId= requestData.get("CustomerId").asInt();

        if (customerDao.isValidCustomer(customerId)) {
            logger.info("Valid Customer");
            for (JsonNode product : products) {
                if (orderValidation(product.get("ProductId").asInt(), product.get("Quantity").asInt())) {
                    logger.info("Have ordered amount in stores");
                    productDao.decreaseProductCount(product.get("ProductId").asInt(), product.get("Quantity").asInt());
                    productsList.add(product);
                    logger.info("Order placementSuccessful");
                } else {
                    logger.error("No amount of orderd Quantity of that product in stores");
                    throw new ProductNotAvailableException("No products available to Order");

                }

            }
            String orderStatus = requestData.get("OrderStatus").asText();
            orderDao.placeAnOrder(productsList, customerId, orderStatus);
        }
        else {
            logger.error("Customer is inactive");
            throw new InactiveCustomerException("Customer is inactive");
        }

    }
    public JsonNode getOrdersByCustomerId(int id) {
        return orderDao.findByCustomerId(id);
    }
    public void changeOrderStatus(JsonNode requestData){
        String orderStatus = requestData.get("OrderStatus").asText();
        int orderId = requestData.get("OrderId").asInt();
        if (orderStatusChangeValidation(orderId,orderStatus)){
            logger.info("Valid Status Change");
            orderDao.changeOrderStatus(orderId,orderStatus);
            logger.info("Status change is successful");
        }
        else {
            logger.error("Cannot do requested Order Status change");
            throw new StatusChangeInvalidException("Cannot Change the Order Status to :"+orderStatus);
        }

    }
    public Boolean orderValidation(int productId,int quantity){
        if (quantity<=productDao.getProductCount(productId)) {
            return true;
        }
        else
        return false;
    }
    public Boolean orderStatusChangeValidation(int orderId,String newStatus){
        String currentStatus=orderDao.getCurrentStatus(orderId);
        if (currentStatus.equals("Pending")){
            if ((newStatus.equals("Inprogress"))|(newStatus.equals("Cancelled"))) {
                return true;
            }
            else return false;

        }else if (currentStatus.equals("Inprogress")){
            if((newStatus.equals("Delivered"))|(newStatus.equals("Cancelled"))){
                return true;
            }
            else return false;
        }
        else return false;


    }


}
