package service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import dao.CustomerDao;
import service.CustomerService;
import org.apache.log4j.Logger;
import java.util.List;

public class CustomerServiceImpl implements CustomerService {
    CustomerDao customerDao;
    Logger logger;
    public CustomerServiceImpl() {
        this.customerDao=new CustomerDao();
        this.logger = Logger.getLogger(CustomerServiceImpl.class);
    }

    public List<Object> getCustomers(){
        return customerDao.getCustomers();

    }
    public void addCustomer(JsonNode requestData){
        String customerName= requestData.get("CustomerName").asText();
        String addressLineOne= requestData.get("AddressLineOne").asText();
        String addressLineTwo= requestData.get("AddressLineTwo").asText();
        String streetNo= requestData.get("StreetNo").asText();
        String landmark= requestData.get("Landmark").asText();
        String city= requestData.get("City").asText();
        String country= requestData.get("Country").asText();
        String zipcode= requestData.get("Zipcode").asText();
        int age=requestData.get("Age").asInt();
        customerDao.addCustomer(customerName,addressLineOne,addressLineTwo,streetNo,landmark,city,country,zipcode,age);
        logger.info("Add Customer Successful");

    }
    public  void deleteCustomer(JsonNode requestData){

        int customerId = requestData.get("customerId").asInt();
        customerDao.deleteCustomer(customerId);
        logger.info("Delete Customer Successful");

    }
}
