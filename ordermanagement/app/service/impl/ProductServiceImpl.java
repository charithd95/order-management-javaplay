package service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import dao.ProductDao;

import org.apache.log4j.Logger;

import java.util.List;

public class ProductServiceImpl {
    ProductDao productDao;
    Logger logger;

    public ProductServiceImpl() {
        this.logger = Logger.getLogger(ProductServiceImpl.class);
        this.productDao=new ProductDao();
    }

    public List<Object> getProducts(){
        logger.info("Get Products Successful");
        return productDao.getProducts();


    }
    public void addProduct(JsonNode requestData){
        String productName= requestData.get("ProductName").asText();
        int quantity = requestData.get("ProductQuantity").asInt();
        double price = requestData.get("ProductPrice").asDouble();
        productDao.addProduct(productName,quantity,price);
        logger.info("Product Addition Successful");

    }
    public  void deleteProduct(JsonNode requestData){

        int productId = requestData.get("productId").asInt();
        productDao.deleteProduct(productId);
        logger.info("Delete Product Successful");

    }

}
