package service;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.List;

public interface ProductService {
    public List<Object> getProducts();
    public void addProduct(JsonNode requestData);
    public  void deleteProduct(JsonNode requestData);
}
