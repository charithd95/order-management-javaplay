package service;

import com.fasterxml.jackson.databind.JsonNode;

public interface OrderService {
    public void placeAnOrder(JsonNode requestData);
    public JsonNode getOrdersByCustomerId(int id);
    public void changeOrderStatus(JsonNode requestData);
}
