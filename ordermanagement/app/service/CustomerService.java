package service;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.List;

public interface CustomerService {
    public void addCustomer(JsonNode requestData);
    public List<Object> getCustomers();
    public  void deleteCustomer(JsonNode requestData);
}
